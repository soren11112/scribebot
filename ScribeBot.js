// Import the discord.js module
const Discord = require('discord.js');
const process = require('process');

// Create an instance of a Discord client
const client = new Discord.Client();

// The token of your bot - https://discordapp.com/developers/applications/me
const token = process.env.TOKEN;



// The ready event is vital, it means that your bot will only start reacting to information
// from Discord _after_ ready is emitted
client.on('ready', () => {
  console.log('I am ready!');

});


// Create an event listener for messages
client.on('message', message => {
  if (message.content.toUpperCase() == 'PING') {
    message.channel.send("Up for " + client.uptime / 3600000 + " hours." );
  }
  /*
  if (message.content.toUpperCase() == '!CLEAR' && (message.member.hoistRole.hasPermission('MANAGE_CHANNELS') || message.member.hoistRole.hasPermission('ADMINISTRATOR'))) {
    //make members object arrays
    var membersmap = message.guild.members.map(m=>m);


    //goes through each user
    for (i = 0; i < membersmap.length; i++){
      var member = membersmap[i];
      if(member.displayName != member.user.username){
        console.log(member.displayName);
        //changes nickname to their default username
        member.setNickname(member.user.username).catch(console.error);
      }

    }

    message.channel.send("Cleared nicknames");
  }
  */

});


// Create an event listener for new guild members
client.on('guildMemberAdd', member => {
  console.log("new member has joined");
  console.log(member);
  // Send the message to a designated channel on a server:
  /*const role = member.guild.roles.get('430194460261875713');*/
  //const channel = member.guild.channels.find('name', 'new-players');
  // Do nothing if the channel wasn't found on this server
  //if (!channel) return;
  /*if (!(!role)){
    member.addRole(role).catch(console.error);
  }*/
  // Send the message, mentioning the member
  
  member.send(`Hi there ${member}! Welcome the PDX RP server! To get started, go to #server-rules to inform yourselves about the rules and regulations here on the server.

 Next, you should go to #server-information  in order to get a thorough understanding of the server.
 Lastly head on over to #role-requests and request the roles for any PDX games you are interested in playing.
 
We wish an amazing time here on the server, and a great time roleplaying with likeminded people!
  `);
});
 
// Log our bot in
client.login(token);